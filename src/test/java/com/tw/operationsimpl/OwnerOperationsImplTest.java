package com.tw.operationsimpl;

import com.tw.common.ShoppingUsersEnum;
import com.tw.operations.OwnerOperations;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class OwnerOperationsImplTest {

    private static OwnerOperations ownerOperations = null;

    @BeforeClass
    public static void init() throws Exception {
        ownerOperations = new OwnerOperationsImpl();
    }

    @Test
    public void searchBrandTestFail() {
        ownerOperations.searchBrand("Livis");
    }

    @Test
    public void searchBrandTestSuccess() {
        ownerOperations.searchBrand("Biba");
    }

    @Test
    public void addBrandTestFail() {
        ownerOperations.addBrand(ShoppingUsersEnum.User, null);
    }

    @Test
    public void addBrandTestSuccess() {
        ownerOperations.addBrand(ShoppingUsersEnum.Owner, new HashMap<String, Map<String, List<String>>>());
    }

    @Test
    public void displayAvailableBrandsTest() {
        ownerOperations.displayAvailableBrands(ownerOperations.availableBrands());
    }
}