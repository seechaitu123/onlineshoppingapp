package com.tw.operationsimpl;

import com.tw.operations.OwnerOperations;
import com.tw.operations.UserOperations;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserOperationsImplTest {
    private static UserOperations userOperations = null;

    @BeforeClass
    public static void init() throws Exception {
        userOperations = new UserOperationsImpl();
    }

    @Test
    public void searchBrandTestFail() {
        userOperations.searchBrand("Livis");
    }

    @Test
    public void searchBrandTestSuccess() {
        userOperations.searchBrand("Biba");
    }


    @Test
    public void addBrandsToCart() {
        userOperations.addBrandsToCart();
    }
}