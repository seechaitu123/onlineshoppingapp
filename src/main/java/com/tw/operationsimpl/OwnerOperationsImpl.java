package com.tw.operationsimpl;

import com.tw.common.ShoppingUsersEnum;
import com.tw.operations.OwnerOperations;
import com.tw.util.ShoppingUtil;

import java.util.List;
import java.util.Map;

public class OwnerOperationsImpl implements OwnerOperations {
    Map<String, Map<String, List<String>>> brands = null;

    public OwnerOperationsImpl() {
        brands = ShoppingUtil.availableBrands();
    }

    public void searchBrand(String brandName) {

        Map<String, Map<String, List<String>>> brands = availableBrands();
        if (brands.containsKey(brandName)) {
            System.out.println("Yes, brand is available");

        } else {
            System.out.println("Sorry, such brand not available");
        }
    }

    public void addBrand(ShoppingUsersEnum user, Map<String, Map<String, List<String>>> brandInfo) {
        if (user.toString().equalsIgnoreCase(ShoppingUsersEnum.Owner.toString())) {
            brands.putAll(brandInfo);
            System.out.println("Success, New brand added");
        } else {
            System.out.println("Sorry, Non Admin role can not add brand");
        }
    }

    public Map<String, Map<String, List<String>>> availableBrands() {
        return brands;
    }

    public void displayAvailableBrands(Map<String, Map<String, List<String>>> brands) {
        ShoppingUtil.displayAvailableBrands(brands);
    }
}