package com.tw.operationsimpl;

import com.tw.operations.UserOperations;
import com.tw.util.ShoppingUtil;

import java.util.List;
import java.util.Map;

public class UserOperationsImpl implements UserOperations {
    public void searchBrand(String brandName) {
        Map<String, Map<String, List<String>>> brands = ShoppingUtil.availableBrands();
        if (brands.containsKey(brandName)) {
            System.out.println("Yes, brand is available");

        } else {
            System.out.println("Sorry, such brand not available");
        }
    }

    public void addBrandsToCart() {
        //--Todo--
        System.out.println(" User added brands to cart : " + ShoppingUtil.availableBrands().keySet().toArray()[0]);
    }
}
