package com.tw.operations;

import com.tw.common.ShoppingUsersEnum;

import java.util.List;
import java.util.Map;

public interface OwnerOperations {
    public void searchBrand(String brandName);
    public void addBrand(ShoppingUsersEnum user, Map<String, Map<String, List<String>>> brandInfo);
    public Map<String, Map<String, List<String>>> availableBrands();
    public void displayAvailableBrands(Map<String, Map<String, List<String>>> brands);
}
