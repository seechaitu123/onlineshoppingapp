package com.tw.operations;

public interface UserOperations {
    public void searchBrand(String brandName);
    public void addBrandsToCart();
}
