package com.tw.util;

import com.tw.operationsimpl.OwnerOperationsImpl;

import java.util.*;

public class    ShoppingUtil {

    public static Map<String, Map<String, List<String>>> availableBrands() {

        Map<String, Map<String, List<String>>> brands = null;
        Map<String, List<String>> items = null;
        List<String> subItems = null;

        brands = new HashMap<String, Map<String, List<String>>>();
        items = new HashMap<String, List<String>>();

        subItems = new ArrayList<String>();
        subItems.add("Shirts");
        subItems.add("Trousers");
        subItems.add("Jerkins");
        subItems.add("Shorts");
        items.put("Women", subItems);

        subItems = new ArrayList<String>();
        subItems.add("Shirts");
        subItems.add("Trousers");
        subItems.add("Jerkins");
        subItems.add("Shorts");
        items.put("Men", subItems);

        subItems = new ArrayList<String>();
        subItems.add("Lipsticks");
        subItems.add("Rings");
        subItems.add("Bangles");
        subItems.add("HairBands");
        items.put("Accessories", subItems);

        brands.put("Biba", items);

        subItems = new ArrayList<String>();
        subItems.add("Shirts");
        subItems.add("Trousers");
        subItems.add("Jerkins");
        subItems.add("Shorts");
        items.put("Women", subItems);

        subItems = new ArrayList<String>();
        subItems.add("Shirts");
        subItems.add("Trousers");
        subItems.add("Jerkins");
        subItems.add("Shorts");
        items.put("Men", subItems);

        subItems = new ArrayList<String>();
        subItems.add("Lipsticks");
        subItems.add("Rings");
        subItems.add("Bangles");
        subItems.add("HairBands");
        items.put("Accessories", subItems);

        brands.put("Aurelia", items);

        return brands;
    }

    public static void displayAvailableBrands(Map<String, Map<String, List<String>>> brands) {
        System.out.println("Available brands are . . .");
        for (Map.Entry<String, Map<String, List<String>>> entry : brands.entrySet()) {
            System.out.println("Brand Name : " + entry.getKey());
            for (Map.Entry<String, List<String>> subEntry : entry.getValue().entrySet()) {
                System.out.println(" \t  Items : " + subEntry.getKey());
                System.out.println(" \t  \t Items List : ");
                for (String list : subEntry.getValue()) {
                    System.out.println(" \t  \t  \t  \t " + list);
                }
            }
        }

    }
}
